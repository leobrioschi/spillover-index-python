# Spillover Index python

Provides a pandas.DataFrame with the Spillover index from _Diebold, F. X. & Yilmaz,K._ 2009. **Measuring Financial Asset Return and Volatility Spillovers, with Application to Global Equity Markets** _(DOI: 10.1111/j.1468-0297.2008.02208.x)_

## What does it do?

It takes a VARResult construct from [Statsmodels VAR(p)](https://www.statsmodels.org/stable/vector_ar.html#var) and uses its Forecast Error Variance Decomposition (FEVD) to produce the table.

## Using the project

The function is provided as is from _spillover.py_ so just import it

```python
from spillover import spillover
```

The first table of returns' spillover in the paper (DOI: 10.1111/j.1468-0297.2008.02208.x) can be replicated with the **tsa.py** application, provided you have the required packages installed.

## Support
Please, any suggestions just e-mail at _leobrioschi [aht] pm (dot) me_

## Roadmap
Diebold and Yilmaz (2012)'s index can be found here:
[https://groups.google.com/g/pystatsmodels/c/BqMqOIghN78/m/A3CpDH6_h5AJ], where I also took as inspiration.

## Authors and acknowledgment
Thanks Charles Martineau and Josef Perktold for the following 2014 discussion [https://groups.google.com/g/pystatsmodels/c/BqMqOIghN78/m/A3CpDH6_h5AJ]. Where I could understand a bit better how statsmodels' VAR(p) works.

I am _Leonardo Brioschi,_ a PhD Student (Accounting/Finance) at _Fucape Business School_ (**Brazil**)

## License
Since I am just reformatting something that the statsmodels already calculates, this is **free** to use as anyway you like, without licenses attached, but you are responsible for checking the results!

## Project status
I am not sure if anything else is needed to be done.


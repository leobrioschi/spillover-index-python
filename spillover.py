# Spillover table as provided in Francis X. Diebold & Kamil Yilmaz, 2009. 
#	"Measuring Financial Asset Return and Volatility Spillovers,
# 		with Application to Global Equity Markets," 
#	Economic Journal, Royal Economic Society, vol. 119(534), January. 
#
# It takes VARresults from statsmodels
#	- "nsteps" is the number of steps forward (forecast)


import pandas as pd
import numpy as np

def spillover(results,nsteps):
    names = results.names
    fevd = results.fevd(nsteps)
    fe = fevd.decomp[:,-1,:]
    fe = fe * 100
    ans = np.round(fe, 2)

    cumulative = np.copy(fe)
    np.fill_diagonal(cumulative, 0)
    cum_rows = np.round(np.sum(cumulative,axis=1),2)
    cum_cols = np.round(np.sum(cumulative,axis=0),2)
    cum_cols_inc_own = np.round(np.sum(fe,axis=0),2)

    size = len(names)
    sp_table = pd.DataFrame(ans,columns=names)
    sp_table = sp_table.append(pd.DataFrame(cum_cols.reshape(1,size),columns=names), ignore_index=True)
    sp_table = sp_table.append(pd.DataFrame(cum_cols_inc_own.reshape(1,size),columns=names), ignore_index=True)
    sp_table['To'] = (names + ['Contribution to'] + ['Including own'])
    cum_rows = np.append(cum_rows,[np.sum(cum_cols),np.sum(cum_cols)/np.sum(cum_cols_inc_own)])
    sp_table['Contribution from'] = cum_rows
    sp_table = sp_table.set_index('To')

    return sp_table

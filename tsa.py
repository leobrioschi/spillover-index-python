# Spillover table recreation from Francis X. Diebold & Kamil Yilmaz, 2009. 
#	"Measuring Financial Asset Return and Volatility Spillovers,
# 		with Application to Global Equity Markets," 
#	Economic Journal, Royal Economic Society, vol. 119(534), January. 
#
# Inspired by:
# 	https://groups.google.com/g/pystatsmodels/c/BqMqOIghN78/m/A3CpDH6_h5AJ
# 		Which solves Diebold & Yilmaz 2012

import statsmodels.api as sm
import statsmodels.tsa.api as tsa
import pandas as pd
import numpy as np
from spillover import spillover

def main():
    
    nsteps=10
    
    #open data
    db = pd.read_excel('dy_dataret.xls')
    db = db.drop('date',axis =1)
    #print(db.head())
    
    model = tsa.VAR(db)
    results = model.fit(2)
    
    #print(spillover(results,nsteps)) #Prints the dataframe
    print(spillover(results,nsteps).to_latex()) #Prints as latex.

if __name__ == '__main__':
        main()
